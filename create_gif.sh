#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 SOURCE_DIRECTORY OUTPUT_FILE" >&2
  exit 1
fi

source_dir="$1"
out_file="$2"

if ! [ -d "$source_dir" ]; then
  echo "$source_dir is not a directory" >&2
  exit 1
fi

ffmpeg -framerate 60 -i "$source_dir/frame_%00d.png" -r 15 -vf "scale=600:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" "$out_file"
