/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::path::Path;
use std::time::Instant;
use std::fs;

use kiss3d::window::Window;
use kiss3d::light::Light;
use kiss3d::nalgebra::{Point3, Vector3, Point2};
use kiss3d::scene::SceneNode;
use kiss3d::text::Font;
use kiss3d::camera::ArcBall;
use rand::{Rng, SeedableRng};
use rand::rngs::StdRng;

const RNG_SEED: u64 = 42;

/// Epsilon (potential well depth) parameter of the Lennard-Jones potential
const LJ_EPSILON: f32 = 5.0f32;
/// Sigma (particle size) parameter of the Lennard-Jones potential
const LJ_SIGMA: f32 = 1.0f32;
/// Coulomb's constant
const K_E: f32 = 1.0 / (4.0 * std::f32::consts::PI * 8.8541878128e-12);
/// Elementary charge
const Q_E: f32 = 1.602176634e-19;
/// Charge of the particles
const Q: f32 = Q_E * 4.45e14;

/// Maximum particle velocity
const VELOCITY_MAX: f32 = 20.0;

/// Simulation end time
const END_TIME: f32 = 2500.0;

/// Whether to save the frames as images
const STORE_FRAMES: bool = false;
/// Only every nth frame is saved
const STORE_NTH: usize = 5;
/// Directory where frames will be saved
const FRAMES_DIR: &str = "frames";

#[derive(Clone)]
struct Particle {
    position: Point3<f32>,
    velocity: Vector3<f32>,
    acceleration: Vector3<f32>,
    charge: f32,
    fixed: bool,
}

fn rand_point3<R: Rng>(r: &mut R, min: Point3<f32>, max: Point3<f32>) -> Point3<f32> {
    Point3::new(
        r.gen_range(min.x..=max.x),
        r.gen_range(min.y..=max.y),
        r.gen_range(min.z..=max.z)
    )
}

#[derive(Debug, PartialEq)]
enum SimPhase {
    Growing,
    Annealing,
}

struct Sim {
    time: f32,
    steps: usize,
    phase: SimPhase,
    /// All particles, double-buffered
    particles: [Vec<Particle>; 2],
    /// Which of the two `particles` Vectors holds the latest data
    particles_select: usize,
    /// The simulation transitions to the annealing phase when this number of particles is reached
    max_particles: usize,
    walls_min: Point3<f32>,
    walls_max: Point3<f32>,
    rng: StdRng,
}

impl Sim {
    fn new(walls_min: Point3<f32>, walls_max: Point3<f32>, max_particles: usize) -> Sim {
        Sim {
            time: 0.0,
            steps: 0,
            phase: SimPhase::Growing,
            particles: [Vec::with_capacity(max_particles), Vec::with_capacity(max_particles)],
            particles_select: 0,
            max_particles,
            walls_min,
            walls_max,
            rng: StdRng::seed_from_u64(RNG_SEED),
        }
    }

    fn add_particle_at(&mut self, pos: Point3<f32>, fixed: bool) {
        if self.phase != SimPhase::Growing {
            return;
        }

        // Move towards origin
        let vel = -Vector3::from(pos.coords).normalize() * 2.0;
        let particle = Particle {
            position: pos,
            velocity: vel,
            acceleration: Vector3::default(),
            // Ensure same number of positively and negatively charged particles
            charge: if self.particles[0].len() % 2 == 0 { Q } else { -Q },
            fixed,
        };
        let particle_copy = particle.clone();
        self.particles[0].push(particle);
        self.particles[1].push(particle_copy);

        if self.particles[0].len() >= self.max_particles {
            self.phase = SimPhase::Annealing;
        }
    }

    fn add_particle(&mut self, fixed: bool) {
        let pos = rand_point3(&mut self.rng, self.walls_min, self.walls_max);
        self.add_particle_at(pos, fixed);
    }

    fn step(&mut self, dt: f32) {
        // Assign `particles` Vectors according to `particles_select` and update it
        let (particles0, particles1) = self.particles.split_at_mut(1);
        let old_particles;
        let new_particles;
        if self.particles_select == 0 {
            self.particles_select = 1;
            new_particles = &mut particles1[0];
            old_particles = &particles0[0];
        } else {
            self.particles_select = 0;
            new_particles = &mut particles0[0];
            old_particles = &particles1[0];
        }

        for (i, new_particle) in new_particles.iter_mut().enumerate() {
            if !new_particle.fixed {
                let old_particle = &old_particles[i];

                let mut new_pos: Point3<f32> = old_particle.position + old_particle.velocity * dt;

                let new_acc: Vector3<f32> = Self::calculate_acceleration(old_particles, old_particle, i);

                let mut new_vel: Vector3<f32> = old_particle.velocity + 0.5 * (old_particle.acceleration + new_acc) * dt;

                // Constrain particles to walls
                if new_pos.x < self.walls_min.x || new_pos.x > self.walls_max.x {
                    new_vel.x = -1.0 * new_vel.x;
                    new_pos.x = new_pos.x.clamp(self.walls_min.x, self.walls_max.x);
                }
                if new_pos.y < self.walls_min.y || new_pos.y > self.walls_max.y {
                    new_vel.y = -1.0 * new_vel.y;
                    new_pos.y = new_pos.y.clamp(self.walls_min.y, self.walls_max.y);
                }
                if new_pos.z < self.walls_min.z || new_pos.z > self.walls_max.z {
                    new_vel.z = -1.0 * new_vel.z;
                    new_pos.z = new_pos.z.clamp(self.walls_min.z, self.walls_max.z);
                }

                // Cool particles
                let damping_factor = if self.phase == SimPhase::Annealing {
                    0.9
                } else {
                    0.5
                };
                new_vel *= 1.0 - dt * damping_factor;

                // Limit velocity
                new_vel[0] = new_vel[0].clamp(-VELOCITY_MAX, VELOCITY_MAX);
                new_vel[1] = new_vel[1].clamp(-VELOCITY_MAX, VELOCITY_MAX);
                new_vel[2] = new_vel[2].clamp(-VELOCITY_MAX, VELOCITY_MAX);

                new_particle.position = new_pos;
                new_particle.velocity = new_vel;
                new_particle.acceleration = new_acc;
            }
        }

        self.time += dt;
        self.steps += 1;
    }

    fn calculate_acceleration(particles: &Vec<Particle>, particle: &Particle, particle_index: usize) -> Vector3<f32> {
        // Pre-calculate coefficients for the Lennard-Jones potential
        let lj_a = 144.0 * LJ_EPSILON * LJ_SIGMA.powi(12);
        let lj_b = 24.0 * LJ_EPSILON * LJ_SIGMA.powi(6);

        let mut sum_x = 0.0;
        let mut sum_y = 0.0;
        let mut sum_z = 0.0;

        for (other_index, other) in particles.iter().enumerate() {
            if other_index != particle_index {
                let pos_diff: Vector3<f32> = particle.position - other.position;
                let r_squared = pos_diff.magnitude_squared();
                let r = r_squared.sqrt();

                // Force due to the particles' electric charge
                let coulomb_force = K_E * (particle.charge * other.charge) / r_squared;

                // Force due to the Lennard-Jones potential (i.e. the first derivative of V_LJ(r))
                let lj_force = lj_a / r.powi(13) - lj_b / r.powi(7);

                let total_force = coulomb_force + lj_force;
                let force_vector = total_force * pos_diff / r;

                sum_x += force_vector[0];
                sum_y += force_vector[1];
                sum_z += force_vector[2];
            }
        }

        Vector3::new(sum_x, sum_y, sum_z)
    }

    fn many_steps(&mut self, dt: f32, count: u32) {
        for _ in 0..count {
            self.step(dt);
        }
    }
}

fn main() {
    let mut window = Window::new("Crystal growth simulation");

    window.set_background_color(1.0, 1.0, 1.0);
    window.set_light(Light::StickToCamera);

    let box_size_half = LJ_SIGMA * 20.0;
    let walls_min = Point3::new(-box_size_half, -box_size_half, -box_size_half);
    let walls_max = Point3::new(box_size_half, box_size_half, box_size_half);
    let mut sim = Sim::new(walls_min, walls_max, 200);
    // Add a fixed nucleation site at the center
    sim.add_particle_at(Point3::origin(), true);

    let mut spheres: Vec<SceneNode> = Vec::new();

    let font = Font::new(&Path::new("fonts/OpenSans-Regular.ttf")).unwrap();

    let mut camera = ArcBall::new(walls_max + Vector3::new(10.0, 10.0, 10.0), Point3::origin());

    let mut prev_frame_time = Instant::now();
    let mut prev_frame_steps = sim.steps;

    let mut next_particle_time = 0.0;

    let store_path = Path::new(FRAMES_DIR);
    if STORE_FRAMES {
        fs::create_dir_all(store_path)
            .expect("Failed to create frames output directory");
    }

    let mut frame_counter = 0;
    while window.render_with_camera(&mut camera) {
        if sim.time < END_TIME {
            // Store every nth frame
            if STORE_FRAMES && frame_counter % STORE_NTH == 0 {
                let frame_image = window.snap_image();
                let file_name = store_path.join(format!("frame_{}.png", frame_counter / STORE_NTH));
                println!("Storing frame {} as {}", frame_counter, file_name.display());
                frame_image.save(file_name)
                    .expect("Failed to save frame to disk");
            }

            // Regularly add new particle
            if sim.time >= next_particle_time {
                sim.add_particle(false);
                next_particle_time = sim.time + 10.0;
            }
            sim.many_steps(0.001, 500);

            let particles = &sim.particles[sim.particles_select];

            // Update existing spheres' positions
            for (i, sphere) in spheres.iter_mut().enumerate() {
                let particle = &particles[i];
                sphere.set_local_translation(particle.position.into());
            }

            // Add new spheres for new particles
            for i in spheres.len()..particles.len() {
                let particle = &particles[i];
                let mut sphere = window.add_sphere(LJ_SIGMA);
                // Map [-2*Q, 2*Q] to [0, 1]
                let charge01 = (particle.charge / Q + 2.0) * 0.25;
                // Positive -> red, negative -> blue
                sphere.set_color(charge01, 0.0, 1.0 - charge01);
                sphere.set_local_translation(particle.position.into());
                spheres.push(sphere);
            }
        }

        let now = Instant::now();
        let frame_duration = now.duration_since(prev_frame_time);
        prev_frame_time = now;

        let steps_diff = sim.steps - prev_frame_steps;
        prev_frame_steps = sim.steps;

        let steps_per_sec = steps_diff as f32 / frame_duration.as_secs_f32();

        window.draw_text(&format!("Simulation time: {:.4}", sim.time), &Point2::new(10.0, 10.0), 30.0, &font, &Point3::new(0.0, 0.0, 0.0));
        let particle_count = sim.particles[sim.particles_select].len();
        window.draw_text(&format!("No. of particles: {}", particle_count), &Point2::new(10.0, 50.0), 30.0, &font, &Point3::new(0.0, 0.0, 0.0));
        window.draw_text(&format!("Phase: {:?}", sim.phase), &Point2::new(10.0, 90.0), 30.0, &font, &Point3::new(0.0, 0.0, 0.0));
        window.draw_text(&format!("{} steps/s", steps_per_sec), &Point2::new(10.0, 130.0), 30.0, &font, &Point3::new(0.0, 0.0, 0.0));

        frame_counter += 1;
    }
}
