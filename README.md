
# crystal-sim

Molecular dynamics (MD) simulation of crystal growth. It doesn't simulate any real material but rather a simplified model where all atoms have the same mass and size and are point charges.

Interatomic forces are modeled with the [Lennard-Jones potential](https://en.wikipedia.org/wiki/Lennard-Jones_potential) and the [Coulomb force](https://en.wikipedia.org/wiki/Coulomb%27s_law).

The program uses the [velocity Verlet](https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet) method to solve the motion equations.

![Animated recording of a simulation run](media/simulation.gif)

*Simulation of 200 particles starting with a single particle and ending with an annealed crystal.*

Inspired by [this video](https://youtu.be/06TscuHNvGQ) by Brian Haidet of AlphaPhoenix.
