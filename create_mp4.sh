#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 SOURCE_DIRECTORY OUTPUT_FILE" >&2
  exit 1
fi

source_dir="$1"
out_file="$2"

if ! [ -d "$source_dir" ]; then
  echo "$source_dir is not a directory" >&2
  exit 1
fi

ffmpeg -framerate 60 -i "$source_dir/frame_%00d.png" -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "$out_file"
